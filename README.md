# PrivateBin - Ansible Role
This role covers deployment, configuration and software updates of PrivateBin. This role is released under MIT Licence and we give no warranty for this piece of software. Currently supported OS - Debian.

You can deploy test instance using `Vagrantfile` attached to the role.

However, to have this role working with Vagrant, you have to get a self-signed certificate. If you don't want it, simply change the var `selfsigned` from `true` to `false`.

Then simply:
`vagrant up`

`ansible-playbook -b Playbooks/privatebin.yml`

Then you can access PrivateBin from your computer on https://192.168.33.12


## Playbook
The playbook includes php-fpm and nginx role and deploys entire stack needed to run PrivateBin. Additional roles are also available in the Ansible roles repos in git.
